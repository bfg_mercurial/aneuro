const config = require('./config.json');
const gulp = require('gulp');
const chalk = require('chalk');
const log = console.log;

const changed = require('gulp-changed');
const data = require('gulp-data');
const debug = require('gulp-debug');
const del = require('del');
const fs = require('fs');
const fl = require('gulp-filelist');
const fileindex = require('gulp-fileindex');
const ftp = require( 'vinyl-ftp' );
const vfs = require('vinyl-fs');
const gutil = require('gulp-util');
const newer = require('gulp-newer');
const notify = require('gulp-notify');
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const sourcemaps = require('gulp-sourcemaps');
const zip = require('gulp-zip');

// browserSync
const browsync = require('browser-sync');
const htmlin = require("bs-html-injector");
const reload = browsync.reload();
// sass
const autoprefixer = require("autoprefixer");
const autopref = require('gulp-autoprefixer');
const mqpacker = require("css-mqpacker");
const postcss = require("gulp-postcss");
const sass = require('gulp-sass');
const gulpif = require('gulp-if');
const cached = require('gulp-cached');
const sassInheritance = require('gulp-sass-inheritance');
const filter = require('gulp-filter');
// const htmlclean = require('gulp-html-remove');
const htmlclean = require('gulp-htmlclean');
const inlineCss = require('gulp-inline-css');
const uncss = require('gulp-uncss');
const nano = require('gulp-cssnano');


//pug
const pug = require('gulp-pug');
const emitty = require('emitty');
const emittyPug = emitty.setup('src', 'pug',{makeVinylFile: true});
//slim
const slim = require('gulp-slim');

//coffee
const coffee = require('gulp-coffee');
const concat = require('gulp-concat');
const filesize = require('gulp-filesize');
const uglify = require('gulp-uglify');

// data
const merge = require('gulp-merge-json');
const path = require('path');

// img_sync
const fsync = require('gulp-files-sync');
const imagemin = require('gulp-imagemin');
const smushit = require('gulp-smushit');

// svg
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');
const svgmin = require('gulp-svgmin');
const svgs	= require('gulp-svg-sprite');
// sprites
const sprites = require('gulp.spritesmith');
const html2pug = require('gulp-html2pug');

gulp.task('symbols', function() {
  return gulp.src('./src/svg/symbols/**/*.svg')
  .pipe(svgmin({
    js2svg: {
      pretty: true
    }
  }))
  .pipe(cheerio({
    run: function ($) {
      // $('[fill]').removeAttr('fill');
      // $('[stroke]').removeAttr('stroke');
      $('[style]').removeAttr('style');
    },
    parserOptions: {xmlMode: true}
  }))
  .pipe(replace('&gt;', '>'))
    .pipe(svgs({
      mode: {
        symbol: {
          dest: './build/svg',
          sprite: 'symbols.svg',
          example: false
        }
      },
      svg: {
        xmlDeclaration: false,
        doctypeDeclaration: false
      }
    }))
    .pipe(gulp.dest('.'));
});

function lazyRequireTask(taskName, path, options) {
  options = options || {};
  options.taskName = taskName;
  gulp.task(taskName, function(callback) {
    let task = require(path).call(this, options);
    return task(callback);
  });
}

lazyRequireTask('browsync', './gulp-tasks/browsync', {
  baseDir: config.paths.html.dest,
});

lazyRequireTask('style', './gulp-tasks/style', {
  error: config.message.sass.error,
  src: config.paths.css.src,
  dest: config.paths.css.dest,
  prefix: ["> 0%", "last 2 versions", "Firefox >= 20", "Opera >= 20", "ios 8"],
  include: ["bower_components"],
  // style: "nested"
  style: "compressed"
});

lazyRequireTask('scripts', './gulp-tasks/scripts', {
  error: config.message.scripts.error,
  src: ['src/scripts/**/*.coffee'],
  dest: 'build/js'
});
lazyRequireTask('zip', './gulp-tasks/zip', {
  src: config.paths.zip.src,
  dest: config.paths.zip.dest
});
lazyRequireTask('ftp', './gulp-tasks/ftp');
lazyRequireTask('svg', './gulp-tasks/svg');
lazyRequireTask('sprites', './gulp-tasks/sprites');

gulp.task('vendors', function() {
  return gulp.src('src/vendors/**/*.js')
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('build/js'))
    .pipe(filesize())
    .pipe(uglify())
    .pipe(rename('vendors.min.js'))
    .pipe(gulp.dest('build/js'))
    .pipe(filesize())
    .on('error', gutil.log)
})
lazyRequireTask('data', './gulp-tasks/data', {
  error: config.message.data.error,
  data: config.paths.tmp.json,
  dest: config.paths.tmp.dest
});

gulp.task('assets', function() {
  return gulp.src('src/assets/{images,css,js,fonts}/**/*.*', {
      since: gulp.lastRun('assets')
    })
    .pipe(newer('build'))
    // .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('build'));
});

gulp.task('imgmin', function() {
  return gulp.src('./src/assets/images/*.{jpg,png}')
    .pipe(newer('./build/images'))
    .pipe(smushit({
      verbose: true
    }))
    .pipe(gulp.dest('./build/images'))
});

gulp.task('uncss', function () {
  return gulp.src('./build/css/style.css')
      .pipe(concat("style.css"))
      .pipe(nano())
      .pipe(rename({
        suffix: ".min"
      }))
      .pipe(gulp.dest('./build/css'));
});

// gulp.task('setWatch', function(callback) {
//   global.isWatching = true;
//   callback()
// });

gulp.task('tmp', () =>
	new Promise((resolve, reject) => {
    const sourceOptions = global.watch ? { read: false } : {};
		emittyPug.scan(global.emittyChangedFile).then(() => {
			return gulp.src('src/pug/*.pug', sourceOptions)
				.pipe(gulpif(global.watch, emittyPug.filter(global.emittyChangedFile)))
        .pipe(data( function(file) {
          return JSON.parse(fs.readFileSync(config.paths.tmp.data))
        }))
				.pipe(pug({ pretty: true }))
        .on('error', notify.onError(err =>({
            tytle: 'slim_Include Error',
            message: "<%= error.message %>"
          })
        ))
				.pipe(gulp.dest('build'))
				.on('end', resolve)
				// .on('error', reject);
		});
	})
);

gulp.task('watch', () => {
  global.watch = true;

  gulp.watch(config.watch.sass, gulp.series('style'))

  gulp.watch(config.watch.scripts, gulp.series('scripts'))

  gulp.watch('./src/vendors/**/*.js', gulp.series('vendors'))

  gulp.watch('./src/svg/symbols/*.svg', gulp.series('symbols'))
  gulp.watch('./src/svg/sprite/*.svg', gulp.series('svg'))
  gulp.watch('./src/assets/images/icons/*.png', gulp.series('sprites'))

  gulp.watch('./src/assets/images/**/*.*', gulp.series('imgmin'))

  gulp.watch(['./src/assets/{css,js,fonts}/*.*'], gulp.series('assets'))

  gulp.watch("./src/data/**/*.json", gulp.series('data'))
  gulp.watch('./src/pug/**/*.pug', gulp.series('tmp'))
		.on('all', (event, filepath) => {
			global.emittyChangedFile = filepath;
		});
})

// clean task
gulp.task('clean', function() {
  return del('build').then(
    log(chalk.red('Delete folder /build/'))
  );
});
// build task
gulp.task('build', gulp.series(
  'tmp', // create html
  'data', // compile and include data json
  'svg', // create icons from svg
  'symbols', // create icons from svg
  'sprites', // create icons from png
  'style', // compile style.css
  'scripts', // compile scripts
  'assets', // copy assets file to build
  'imgmin' // copy assets file to build
))
// develop task
gulp.task('default', gulp.series(gulp.parallel('browsync', 'watch')))
// deploy task
gulp.task('relise', gulp.series('clean','build','ftp'))
// upload task
gulp.task('upload', gulp.series('ftp'))