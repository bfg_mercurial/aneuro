$(document).ready(->

  $(".scrollBar").mCustomScrollbar
    axis: 'x'
    mouseWheel:
      enable: false
    scrollAmount: 250
    # documentTouchScroll: true
    # advanced:
    #   autoExpandHorizontalScroll: true

  # $('[data-fancybox="gallery"]').fancybox
  #   infobar : false
  #   animationEffect : "zoom-in-out"
  #   # toolbar : true
  #   loop: false
  #   arrows: true
  #   baseClass: "fancy-license"
  #   buttons: [
  #     "close"
  #     ]

  # $('.iCheck').iCheck()

  # $('.slider').slick
  #   prevArrow: "<span class='slick-prev'>\n
  #   <i class='fa fa-angle-left'></i><span>"
  #   nextArrow: "<span class='slick-next'>\n
  #   <i class='fa fa-angle-right'></i><span>"
  #   slidesToShow: 1
  #   slidesToScroll: 1
  #   arrows: false
  #   dots: false
  #   infinite: true
  #   swipeToSlide: false
  #   swipe: false
  #   autoplay: true
  #   autoplaySpeed: 10000


  sliderInit = () ->

    $('.carousel').each (->
      if $(@).hasClass 'carousel-0'
        $(@).slick
          dots: true
          slidesToShow: 3
          slidesToScroll: 1
          centerPadding: 0
          rows: 0
          prevArrow: "<span class='slick-prev'>\n
          <i class='arrow-prev'></i><i class='arr-prev'></i></span>"
          nextArrow: "<span class='slick-next'>\n
          <i class='arrow-next'></i><i class='arr-next'></i></span>"
          responsive: [
            {
              breakpoint: 992,
              settings:
                slidesToShow: 2
                slidesToScroll: 2
            }
            {
              breakpoint: 768,
              settings:
                slidesToShow: 1
                slidesToScroll: 1
                dots: true
                arrows: false
            }
        ]
      else if $(@).hasClass 'carousel-history'
        $(@).slick
          dots: true
          slidesToShow: 4
          slidesToScroll: 1
          centerPadding: 0
          rows: 0
          prevArrow: "<span class='slick-prev'>\n
          <i class='arrow-prev'></i><i class='arr-prev'></i></span>"
          nextArrow: "<span class='slick-next'>\n
          <i class='arrow-next'></i><i class='arr-next'></i></span>"
          responsive: [
            {
              breakpoint: 992,
              settings:
                slidesToShow: 3
                slidesToScroll: 3
            }
            {
              breakpoint: 768,
              settings:
                slidesToShow: 2
                slidesToScroll: 2
                dots: true
                arrows: false
            }
            {
              breakpoint: 480,
              settings:
                slidesToShow: 1
                slidesToScroll: 1
                dots: true
                arrows: false
            }
        ]
      else if $(@).hasClass 'carousel-spectrum'
        $(@).slick
          dots: true
          slidesToShow: 1
          slidesToScroll: 1
          centerPadding: 0
          rows: 0
          arrows: false
#          responsive: [
#            {
#              breakpoint: 992,
#              settings:
#                slidesToShow: 3
#                slidesToScroll: 3
#            }
#            {
#              breakpoint: 768,
#              settings:
#                slidesToShow: 2
#                slidesToScroll: 2
#                dots: true
#                arrows: false
#            }
#            {
#              breakpoint: 480,
#              settings:
#                slidesToShow: 1
#                slidesToScroll: 1
#                dots: true
#                arrows: false
#            }
#        ]

      else
        $(@).slick()
    )

  sliderInit()

  $('input.phone').inputmask("+7 (999) 999-99-99")

  $('.file').each ->
    $input = $(this)
    $label = $input.next('label')
    labelVal = $label.html()

    $input.on 'change', (e) ->
      fileName = ''
      if @files and @files.length > 1
        fileName = (@getAttribute('data-multiple-caption') or '').replace('{count}', @files.length)
      else if e.target.value
        fileName = e.target.value.split('\\').pop()
      if fileName
#        $label.find('span').html fileName
        document.getElementById("link-file").value = fileName
      else
        $label.html labelVal
      return
    # Firefox bug fix

    $input.on('focus', ->
      $input.addClass 'has-focus'
      return
    ).on 'blur', ->
      $input.removeClass 'has-focus'
      return
    return

).on('click', '.upload', (event) ->
  event.preventDefault()
  $(@).next().find('input[type=file]').trigger('click')
)

