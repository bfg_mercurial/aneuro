slider = $("#slider")
sliderClass = $(".slider")
if $("#slider")[0]
  slider = document.getElementById('slider')
  noUiSlider.create slider,
    start: [ 1 ]
    step: 1
    animate: true
    animationDuration: 200
    connect: [true, false]
    range:
      'min': [ 1 ]
      'max': [ 4 ]
    format: wNumb
      decimals: 0

  slider.noUiSlider.on 'start', (values,handle) ->
    value = values[handle]
    n = value - 1
    $('.point-text').removeClass 'isOpen'

  slider.noUiSlider.on 'set', (values,handle) ->
    value = values[handle]
    n = value - 1
    console.log value
    sliderClass.slick('slickGoTo', parseInt(n))
    $(".slider-points .item").removeClass 'active'
    $(".slider-points .item").eq(n).addClass 'active'
    $('.slider-points .item .point-text').removeClass 'isOpen'
    $(".slider-points .item").eq(n).find('.point-text').addClass 'isOpen'
    linewidth(n)