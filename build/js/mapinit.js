var LoadMap, markers;

markers = [
  {
    'title': 'Analogmusiс',
    'lat': '55.7692899',
    'lng': '37.6222192',
    'description': '<h3>Analogmusiс</h3><p><a href="#">www.analogmusic.ru</a>'
  }, {
    'title': 'POP - MUSIC MUSIC',
    'lat': '55.7492899',
    'lng': '37.6022192',
    'description': '<h3>POP - MUSIC MUSIC</h3><p><a href="#">www.pop-music.ru</a></p>'
  }, {
    'title': 'Galaxmedia',
    'lat': '55.7292899',
    'lng': '37.6322192',
    'description': '<h3>Galaxmedia</h3><p><a href="#">www.galaxmedia.ru</a></p>'
  }, {
    'title': 'MUZbazar',
    'lat': '55.7492899',
    'lng': '37.6522192',
    'description': '<h3>MUZbazar</h3><p><a href="#">www.muzbazar.pro</a></p>'
  }
];

LoadMap = function() {
  var data, i, image, infoBubble, map, mapOptions, marker, myLatlng, styledMap, styles;
  styles = [
    {
      stylers: [
        {
          // hue: '#000000'
        }, {
          // saturation: 0
        }
      ]
    }, {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [
        {
          lightness: 100
        }, {
          visibility: 'simplified'
        }
      ]
    }, {
      featureType: 'road',
      elementType: 'labels',
      stylers: [
        {
          // visibility: 'off'
        }
      ]
    }
  ];
  styledMap = new google.maps.StyledMapType(styles, {
    name: 'Styled Map'
  });
  mapOptions = {
    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
    zoom: 12,
    draggable: false,
    scrollwheel: false,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_BOTTOM
    }
  };
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
  image = {
    url: 'images/point.png'
  };
  infoBubble = new InfoBubble({
    shadowStyle: 0,
    padding: 0,
    backgroundColor: '#fff',
    borderRadius: 0,
    arrowSize: 8,
    borderWidth: 1,
    borderColor: '#d73727',
    disableAutoPan: true,
    hideCloseButton: true,
    backgroundClassName: 'phoney',
    arrowStyle: 1,
    disableAnimation: false
  });
  i = 0;
  while (i < markers.length) {
    data = markers[i];
    myLatlng = new google.maps.LatLng(data.lat, data.lng);
    marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      icon: image,
      title: data.title,
      animation: google.maps.Animation.DROP
    });
    (function(marker, data) {
      google.maps.event.addListener(marker, 'mouseover', function(e) {
        // marker.setIcon('images/point_a.png');
        infoBubble.setContent('<div class=\'markers__info\'>' + data.description + '</div>');
        infoBubble.open(map, this);
      });
      google.maps.event.addListener(marker, 'mouseout', function(e) {
        marker.setIcon('images/point.png');
        infoBubble.close();
      });
    })(marker, data);
    i++;
  }
};

google.maps.event.addDomListener(window, 'load', LoadMap);
google.maps.event.addDomListener(window, 'resize', LoadMap);