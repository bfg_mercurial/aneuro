$(document).ready(function() {
  var sliderInit;
  $(".scrollBar").mCustomScrollbar({
    axis: 'x',
    mouseWheel: {
      enable: false
    },
    scrollAmount: 250
  });
  // documentTouchScroll: true
  // advanced:
  //   autoExpandHorizontalScroll: true

  // $('[data-fancybox="gallery"]').fancybox
  //   infobar : false
  //   animationEffect : "zoom-in-out"
  //   # toolbar : true
  //   loop: false
  //   arrows: true
  //   baseClass: "fancy-license"
  //   buttons: [
  //     "close"
  //     ]

  // $('.iCheck').iCheck()

  // $('.slider').slick
  //   prevArrow: "<span class='slick-prev'>\n
  //   <i class='fa fa-angle-left'></i><span>"
  //   nextArrow: "<span class='slick-next'>\n
  //   <i class='fa fa-angle-right'></i><span>"
  //   slidesToShow: 1
  //   slidesToScroll: 1
  //   arrows: false
  //   dots: false
  //   infinite: true
  //   swipeToSlide: false
  //   swipe: false
  //   autoplay: true
  //   autoplaySpeed: 10000
  sliderInit = function() {
    return $('.carousel').each((function() {
      if ($(this).hasClass('carousel-0')) {
        return $(this).slick({
          dots: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          centerPadding: 0,
          rows: 0,
          prevArrow: "<span class='slick-prev'>\n <i class='arrow-prev'></i><i class='arr-prev'></i></span>",
          nextArrow: "<span class='slick-next'>\n <i class='arrow-next'></i><i class='arr-next'></i></span>",
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false
              }
            }
          ]
        });
      } else if ($(this).hasClass('carousel-history')) {
        return $(this).slick({
          dots: true,
          slidesToShow: 4,
          slidesToScroll: 1,
          centerPadding: 0,
          rows: 0,
          prevArrow: "<span class='slick-prev'>\n <i class='arrow-prev'></i><i class='arr-prev'></i></span>",
          nextArrow: "<span class='slick-next'>\n <i class='arrow-next'></i><i class='arr-next'></i></span>",
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true,
                arrows: false
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false
              }
            }
          ]
        });
      } else if ($(this).hasClass('carousel-spectrum')) {
        return $(this).slick({
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: 0,
          rows: 0,
          arrows: false
        });
      } else {
        //          responsive: [
        //            {
        //              breakpoint: 992,
        //              settings:
        //                slidesToShow: 3
        //                slidesToScroll: 3
        //            }
        //            {
        //              breakpoint: 768,
        //              settings:
        //                slidesToShow: 2
        //                slidesToScroll: 2
        //                dots: true
        //                arrows: false
        //            }
        //            {
        //              breakpoint: 480,
        //              settings:
        //                slidesToShow: 1
        //                slidesToScroll: 1
        //                dots: true
        //                arrows: false
        //            }
        //        ]
        return $(this).slick();
      }
    }));
  };
  sliderInit();
  $('input.phone').inputmask("+7 (999) 999-99-99");
  return $('.file').each(function() {
    var $input, $label, labelVal;
    $input = $(this);
    $label = $input.next('label');
    labelVal = $label.html();
    $input.on('change', function(e) {
      var fileName;
      fileName = '';
      if (this.files && this.files.length > 1) {
        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
      } else if (e.target.value) {
        fileName = e.target.value.split('\\').pop();
      }
      if (fileName) {
        //        $label.find('span').html fileName
        document.getElementById("link-file").value = fileName;
      } else {
        $label.html(labelVal);
      }
    });
    // Firefox bug fix
    $input.on('focus', function() {
      $input.addClass('has-focus');
    }).on('blur', function() {
      $input.removeClass('has-focus');
    });
  });
}).on('click', '.upload', function(event) {
  event.preventDefault();
  return $(this).next().find('input[type=file]').trigger('click');
});

//# sourceMappingURL=main.js.map
