## Installing gulp 4 globally
``` bash
  $ npm rm -g gulp
  $ npm install -g gulp-cli
  $ gulp -v
  CLI version 1.2.1
```

### Install gulp 4 locally
```bash
$ npm uninstall gulp --save-dev
$ npm install --save-dev gulp
```
### Next step *
```bash
  $ npm i
```
* (Python 2.7) - обязательно + PATH
or set
```bash
npm i --save-dev chalk coffee-script del autoprefixer browser-sync bs-html-injector css-mqpacker emitty fs gulp-autoprefixer gulp-cache gulp-cached gulp-changed gulp-cheerio gulp-clean gulp-clean-css gulp-coffee gulp-concat gulp-cssnano gulp-csso gulp-data gulp-debug gulp-deploy-ftp gulp-directory-sync gulp-fileindex gulp-filelist gulp-files-sync gulp-filesize gulp-filter gulp-html2pug gulp-htmlclean gulp-if gulp-imagemin gulp-inline-css gulp-invipo-deploy gulp-load-plugins gulp-merge-json gulp-minify gulp-newer gulp-notify gulp-plumber gulp-postcss gulp-pug gulp-rename gulp-replace gulp-run-sequence gulp-sass gulp-sass-inheritance gulp-size gulp-slim gulp-sourcemaps gulp-stats gulp-svg-sprite gulp-svgmin gulp-task-loader gulp-uglify gulp-uncss gulp-util gulp-watch gulp-zip gulp.spritesmith jstransformer-coffee-script jstransformer-markdown-it load-gulp-config path postcss-sorting stream-combiner2 timereport vinyl-fs vinyl-ftp gulp-buffer gulp-spritesmash gulp-smushit
```