const $ = require('gulp-load-plugins')();
const config = require('../config.json');
const gulp = require('gulp');
const gutil = require('gulp-util')
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  return function() {
    return combine(
      gulp.src(options.src),
      $.changed('build/js', {
        extension: '.js'
      }),
      $.sourcemaps.init(),
      $.coffee({
        bare: true
      }).on('error', gutil.log),
      $.sourcemaps.write('./', {
        includeContent: false,
        sourceRoot: './src/scripts'
      }),
      gulp.dest(options.dest)
    ).on('error', $.notify.onError({
      title  : "SCIPRTS compile Error!"
    }))
  }
}