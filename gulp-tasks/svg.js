// const $ = require('gulp-load-plugins')();
const config = require('../config.json');
const gulp = require('gulp');
const svgs	= require('gulp-svg-sprite');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  return function() {
    return gulp.src('./src/svg/sprite/**/*.svg')
      .pipe(svgmin({
        js2svg: {
          pretty: true
        }
      }))
      .pipe(cheerio({
        run: function ($) {
          // $('[fill]').removeAttr('fill');
          // $('[stroke]').removeAttr('stroke');
          // $('[style]').removeAttr('style');
        },
        parserOptions: {xmlMode: true}
      }))
      .pipe(replace('&gt;', '>'))
      .pipe(svgs({
        shape: {
          spacing: {
            padding: 2
          }
        },
        mode: {
          css: {
            dest: 'build',
            prefix: '.svg-',
            sprite: '../svg/sprite.svg',
            dimensions: true,
            bust: false,
            render: {
              sass: {
                dest: '../../src/sass/util/_svg-sprite.sass',
                template: "src/sass/util/_svg_sprite_template.sass"
              }
            }
          }
        }
       }))
      .pipe(gulp.dest('./build'));
  }
}