const config = require('../config.json');
const gulp = require('gulp');
const browsync = require('browser-sync');

module.exports = function(options) {
    return function() {
        browsync.init({
            server: {
                baseDir: config.paths.html.dest
            },
            notify: false,
            open: false,
            browser: [""],
            files: ["./build/js/*.js", "./build/*.html", "./build/css/*.css","./build/images/*.*"],
            plugins: [{
                module: "bs-html-injector",
                options: {
                    files: ["./build/*.html"]
                }
            }]
        })
    }
}