const $ = require('gulp-load-plugins')();
const config = require('../config.json');
const gulp = require('gulp');

const gulpif = require('gulp-if');

const autoprefixer = require("autoprefixer");
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  return function() {
    return combine(
      gulp.src('src/sass/style.sass')
        .pipe(gulpif(global.isWatching, $.cached('sass'))),
        // $.sassInheritance({dir: './src/sass/'}),
        $.filter(function (file) {
          return !/\/_/.test(file.path) || !/^_/.test(file.relative);
        }),
        $.sourcemaps.init(),
        $.sass({
          outputStyle: options.style,
          errLogToConsole: true,
          includePaths: options.include
        }),
        $.postcss(
          plugins = [
          autoprefixer({
            browsers: options.prefix,
            cascade: true,
            mqpacker: true,
            filter: true
          })
        ]),
        $.sourcemaps.write('./', {
          includeContent: false,
          sourceRoot: './src/sass'
        }),
        gulp.dest(options.dest)
      ).on('error', $.notify.onError(err =>({
        tytle: 'SASS Erorr',
        message: "<%= error.message %>"
      })
    ))
  };
};