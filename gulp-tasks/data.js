const $ = require('gulp-load-plugins')();
const config = require('../config.json');
const gulp = require('gulp');
const path = require('path');
const merge = require('gulp-merge-json');
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  return function() {
    return combine(
      gulp.src(options.data),
        $.plumber({
          errorHandler: $.notify.onError({
            message: "<%= error.message %>",
            title  : "JSON Error!"
            })
        })
        .pipe(merge({
          fileName: 'data.json',
            edit: (parsedJson, file) => {
            if (parsedJson.someValue) { delete parsedJson.otherValue;}
            return parsedJson;
          },
        })),
        gulp.dest('./src/pug/data')
      )
      .on('error', $.notify.onError({
        message: "<%= error.message %>",
        title  : "JSON Error!"
      }))
  }
}