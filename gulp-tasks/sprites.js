const config = require('../config.json');
const gulp = require('gulp');
const spritesmith = require('gulp.spritesmith');
const buffer = require('gulp-buffer');
const spritesmash = require('gulp-spritesmash');
const notify = require('gulp-notify');
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  return function(callback) {
    gulp.src('./src/assets/images/icons/*.png')
      .pipe(spritesmith({
        imgName: 'sprites.png',
        imgPath: '../images/sprites.png',
        cssName: '../../src/sass/util/_sprites-icons.sass',
        padding: 20,
        cssTemplate: './src/sass/util/sass.template.handlebars'
        // cssVarMap: function(sprite) {
        //   sprite.name = 's-' + sprite.name
        // }
        })
      )
    .pipe(buffer())
    // .pipe(spritesmash())
    .pipe(gulp.dest('./build/images/'))
    .pipe(notify("Создан новый спрайт!"))

    callback()
  }
}
