const $ = require('gulp-load-plugins')();
const config = require('../config.json');
const gulp = require('gulp');
const fs = require('fs');
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  return function() {
    return combine(
      gulp.src(options.src),
        $.zip('master.zip'),
        gulp.dest(options.dest)
    )
  }
}