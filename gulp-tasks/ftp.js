const config = require('../config.json');
const gulp = require('gulp');
const ftp = require( 'vinyl-ftp');
const gutil = require('gulp-util')
const combine = require('stream-combiner2').obj;

module.exports = function(options) {
  var conn = ftp.create({
    host: '95.213.209.30',
    user: 'frontend',
    password: '7S4y3B2g',
    parallel: 10,
    log: gutil.log
  });
  return function() {
    return combine(
      gulp.src(['./build/**/*.*'], {
        base: './build',
        buffer: false
      })
        .pipe(conn.newer(''))
        .pipe(conn.dest('/www/frontend.clientus.ru/aneuro'))
    )}
}